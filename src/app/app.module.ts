import { RoutingModule } from './modules/routing/routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
import { AppComponent } from './app.component';
import { ChatViewComponent } from './components/chat-view/chat-view.component';
import { LoginViewComponent } from './components/login-view/login-view.component';
import { RegisterViewComponent } from './components/register-view/register-view.component';
import { RestApiServiceService } from './services/rest-api-service.service';
import { HttpClientModule } from '@angular/common/http';
import { PersonSearchViewComponent } from './components/person-search-view/person-search-view.component';
import { ContactListViewComponent } from './components/contact-list-view/contact-list-view.component';
import { ChatListViewComponent } from './components/chat-list-view/chat-list-view.component';
import { BlockedContactListViewComponent } from './components/blocked-contact-list-view/blocked-contact-list-view.component';
import { ContactDataService } from './services/contact-data.service';
import { LoginserviceService } from './services/loginservice.service';
import { ChatListService } from './services/chat-list.service';
import { SocketCommunicationService } from "./services/socket-communication.service";

@NgModule({
  declarations: [
    AppComponent,
    ChatViewComponent,
    LoginViewComponent,
    RegisterViewComponent,
    PersonSearchViewComponent,
    ContactListViewComponent,
    ChatListViewComponent,
    BlockedContactListViewComponent,

  ],
  imports: [
    BrowserModule,
    RoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [{ provide: APP_BASE_HREF, useValue: '/' }, RestApiServiceService, LoginserviceService, ChatListService, SocketCommunicationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
