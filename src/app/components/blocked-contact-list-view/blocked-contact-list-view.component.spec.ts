import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockedContactListViewComponent } from './blocked-contact-list-view.component';
import { AppModule } from '../../app.module';

describe('BlockedContactListViewComponent', () => {
  let component: BlockedContactListViewComponent;
  let fixture: ComponentFixture<BlockedContactListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockedContactListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
