import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatListViewComponent } from './chat-list-view.component';
import { AppModule } from '../../app.module';
import { LoginserviceService } from '../../services/loginservice.service';
import { RestApiServiceService } from '../../services/rest-api-service.service';
import { HttpClient } from '@angular/common/http';
import { ChatListService } from '../../services/chat-list.service';

describe('ChatListViewComponent', () => {
  let component: ChatListViewComponent;
  let fixture: ComponentFixture<ChatListViewComponent>;



  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]

    })
      .compileComponents();
    fixture = TestBed.createComponent(ChatListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
