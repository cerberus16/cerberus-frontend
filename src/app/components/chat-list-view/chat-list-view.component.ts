import { Component, OnInit } from '@angular/core';
import { LoginserviceService } from '../../services/loginservice.service';
import { Chat } from '../../models/Chat';
import { RestApiServiceService } from '../../services/rest-api-service.service';
import { User } from '../../models/user';
@Component({
  selector: 'app-chat-list-view',
  templateUrl: './chat-list-view.component.html',
  styleUrls: ['./chat-list-view.component.css']
})
export class ChatListViewComponent implements OnInit {

  constructor(public loginserviceService: LoginserviceService, public restApiService: RestApiServiceService) {
    //this.laden();
  }

  chats: Chat[];
  lastTime: Date;

  /*laden() {
    this.restApiService.Chatlist(new User("Abdi"));
    console.log("in der Komponente");
  }*/

  get data(): any {
    return this.restApiService.chatCache;
  }
  unlogCall() {
    this.loginserviceService.unlog();
  }


  ngOnInit() {

  }

}

