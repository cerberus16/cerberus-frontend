import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatViewComponent } from './chat-view.component';
import { AppModule } from '../../app.module';

describe('LoginViewComponent', () => {
  let component: ChatViewComponent;
  let fixture: ComponentFixture<ChatViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AppModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should save chatMessage', () => {
    component.sendChatMessage('Hallo');

    expect(component.chatHistory[0]).toBe('Hallo');
  });

  it('should save multiple chatMessages', () => {
    component.sendChatMessage('Hallo');
    expect(component.chatHistory[0]).toBe('Hallo');
    component.sendChatMessage('Welt');
    expect(component.chatHistory[1]).toBe('Welt');
    component.sendChatMessage('Lucas');
    expect(component.chatHistory[2]).toBe('Lucas')
  });



});
