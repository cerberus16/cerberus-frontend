import { Component, OnInit } from '@angular/core';
import { LoginserviceService } from '../../services/loginservice.service';
import { Message } from '../../models/Message';
import { RestApiServiceService } from '../../services/rest-api-service.service';
import { Chat } from '../../models/Chat';
import { ChatListService } from '../../services/chat-list.service';
import { SocketCommunicationService } from "../../services/socket-communication.service";

@Component({
  selector: 'app-chat-view',
  templateUrl: './chat-view.component.html',
  styleUrls: ['./chat-view.component.css']
})
export class ChatViewComponent implements OnInit {

  chatHistory: string[] = [];
  public i: number = 0;
  msg: Message = new Message();
  chats: Message[];

  // tslint:disable-next-line:max-line-length
  constructor(private socketCommunicationService: SocketCommunicationService, private loginserviceService: LoginserviceService, public restApiServiceService: RestApiServiceService, public chatListService: ChatListService) { }

  ngOnInit() {
    //this.restApiServiceService.getGlobal();
    //this.chats = this.chatListService.globalChat;



  }

  get globalChats(): Message[] {
    return this.socketCommunicationService.globalChatListCache;
  }

  sendChatMessage(message: string) {
    this.chatHistory[this.i] = message;
    this.i++;
  }

  async sendMessage(msg) {
    //alert(msg.text);
    // await this.restApiServiceService.sendGlobalChat(msg);
    // this.msg = new Message();
    // console.log(JSON.stringify(this.chats));
    // await this.restApiServiceService.getGlobal();
    // this.chats = this.chatListService.globalChat;
    this.socketCommunicationService.postMessage(msg);
    this.msg = new Message();
  }

  unlogCall() {
    this.loginserviceService.unlog();
  }

}
