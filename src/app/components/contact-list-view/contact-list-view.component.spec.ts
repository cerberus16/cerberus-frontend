import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { ContactListViewComponent } from './contact-list-view.component';
import { Contact } from '../../models/contact';
import { ContactDataService } from '../../services/contact-data.service';
import { FormsModule } from '@angular/forms';


class ContactDataServiceMock {

  private mockContacts: Contact[] = []

  constructor() {
    this.mockContacts.push(new Contact('Mohsii', false, 'eddie'));
    this.mockContacts.push(new Contact('Lukas', false, 'eddie'));
  }

  /* public block(c: Contact) {
     c.blocked = true;
   }*/

  get contacts(): Contact[] {
    return this.mockContacts;
  }
}

describe('ContactListViewComponent', () => {
  let component: ContactListViewComponent;
  let fixture: ComponentFixture<ContactListViewComponent>;
  let conService: ContactDataService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ContactListViewComponent],
      providers: [{ provide: ContactDataService, useClass: ContactDataServiceMock }]
    })
      .compileComponents();
    conService = TestBed.get(ContactDataService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /*it('should block a contact', () => {
    //Hier muss noch gearbeitet werden
    let contacts = conService.contacts;
    console.log(contacts.length);
   // expect(contacts[0].blocked).toBeFalsy();
    component.block(contacts[0]);
    let contactsAfterBlock = conService.contacts;
    // expect(contacts.length - 1).toBe(contactsAfterBlock);
    expect(contactsAfterBlock[0].blocked).toBeTruthy();
  });*/

});
