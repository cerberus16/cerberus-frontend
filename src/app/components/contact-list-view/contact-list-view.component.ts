import { Component, OnInit } from '@angular/core';
import { Contact } from '../../models/contact';
import { RestApiServiceService } from '../../services/rest-api-service.service';
import { ContactDataService } from '../../services/contact-data.service';
import { User } from '../../models/user';
import { LoginserviceService } from '../../services/loginservice.service';

@Component({
  selector: 'app-contact-list-view',
  templateUrl: './contact-list-view.component.html',
  styleUrls: ['./contact-list-view.component.css']
})
export class ContactListViewComponent implements OnInit {


  constructor(private loginserviceService: LoginserviceService, private restApiServiceService: RestApiServiceService) {
    this.restApiServiceService.showContact();

  }
  /*
  Soll rein wenn der Data Service wieder funktioniert.
  constructor(public contactDataService: ContactDataService) {
*/

  user: User = new User();

  ngOnInit() {
  }

  unlogCall() {
    this.loginserviceService.unlog();
  }

  get contacts(): User[] {
    return this.restApiServiceService.contactsCache;
  }

  delContact(delUser: String) {
    this.restApiServiceService.delContact(delUser);
  }

  /*ownContact(contact: Contact) {
    return this.ownContact;
  }

  block(contact: Contact) {
    return this.contactDataService.blocked(contact);
  }

  get noContacts(): boolean {
    return this.contactDataService.noContacts();
  }
  */
}
