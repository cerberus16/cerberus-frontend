import { RoutingModule } from '../../modules/routing/routing.module';
import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { RestApiServiceService } from '../../services/rest-api-service.service';
import { RouterLink } from '@angular/router/src/directives/router_link';
import { LoginserviceService } from '../../services/loginservice.service';

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.css']
})
export class LoginViewComponent implements OnInit {
  usernameHistory: string;
  truePassword: string;
  loginPath: string;
  constructor(public restApiServiceService: RestApiServiceService, public loginserviceService: LoginserviceService) { }

  ngOnInit() {
  }

  // tslint:disable-next-line:member-ordering
  user: User = new User();

  submit(newuser: User) {
    this.restApiServiceService.login(newuser);

    this.user = new User();
  }
}
