import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonSearchViewComponent } from './person-search-view.component';
import { AppModule } from '../../app.module';

describe('PersonSearchViewComponent', () => {
  let component: PersonSearchViewComponent;
  let fixture: ComponentFixture<PersonSearchViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonSearchViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
