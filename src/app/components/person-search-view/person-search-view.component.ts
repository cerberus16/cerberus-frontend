import { Component, OnInit } from '@angular/core';
import { Contact } from '../../models/contact';
import { RestApiServiceService } from '../../services/rest-api-service.service';
import { User } from '../../models/user';
import { LoginserviceService } from '../../services/loginservice.service';

@Component({
  selector: 'app-person-search-view',
  templateUrl: './person-search-view.component.html',
  styleUrls: ['./person-search-view.component.css']
})
export class PersonSearchViewComponent implements OnInit {

  constructor(public restApiServiceService: RestApiServiceService, private loginserviceService: LoginserviceService) {

  }

  ngOnInit() {
  }
  contact: Contact = new Contact();
  username: string;
  user: User = new User();


  submit(newcontact: Contact) {
    this.getUser(this.username);
    this.restApiServiceService.storeContactWithApi(newcontact);
    this.contact = new Contact();
  }

  getUser(username: string) {
    this.contact.username = username;
    this.contact.usernameOwner;
    this.contact.blocked = false;
  }
  unlogCall() {
    this.loginserviceService.unlog();
  }
}
