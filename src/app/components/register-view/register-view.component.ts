import { Component, OnInit } from '@angular/core';
import { RestApiServiceService } from '../../services/rest-api-service.service';
import { User } from '../../models/user';
import { RouterLink } from '@angular/router/src/directives/router_link';
import { importType } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-register-view',
  templateUrl: './register-view.component.html',
  styleUrls: ['./register-view.component.css']
})
export class RegisterViewComponent implements OnInit {

  constructor(public restApiServiceService: RestApiServiceService) {


  }

  ngOnInit() {
  }

  // tslint:disable-next-line:member-ordering
  user: User = new User();

  save(newuser: User) {
    this.restApiServiceService.saveUser(newuser);
    this.user = new User();
  }

}
