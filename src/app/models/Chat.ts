import { Message } from "./Message";


export class Chat {
    // tslint:disable-next-line:max-line-length
    constructor(public currentUser: String, public otherUser: String, public messages: Message[]) { }
}
