import { Message } from "./Message";
import { Chat } from './Chat';
import { User } from "./user";


export class ChatList {
    // tslint:disable-next-line:max-line-length
    constructor(public chats: Chat[]) {
    }

    transformChatByUser(messages: Message[]): Chat[] {
        let chats = new Array<Chat>();

        let names = messages.map(m => m.currentUser);
        messages.map(m => m.otherUser).forEach(n => names.push(n));
        let uniquenames = new Set<String>(names);

        uniquenames.forEach(n => {
            let chat = new Chat('Abdi', n, messages.filter(m => m.otherUser === n || m.currentUser === n));
            chats.push(chat);
            // messages.filter(m => m.otherUser === n || m.currentUser === n);
        });


        return chats;

    }


}
