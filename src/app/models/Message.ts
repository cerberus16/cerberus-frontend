export class Message {
    // tslint:disable-next-line:max-line-length
    constructor(public currentUser?: string, public otherUser?: string, public text?: string, public time?: Date) { }
}