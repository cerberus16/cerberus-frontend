export class Contact {
    // tslint:disable-next-line:max-line-length
    constructor(
        public username?: string,
        public blocked?: boolean,
        public usernameOwner?: string
    ) { }
}
