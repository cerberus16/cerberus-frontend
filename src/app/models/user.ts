export class User {
    // tslint:disable-next-line:max-line-length
    constructor(public username?: string,
        public password?: string,
        public gebDat?: Date,
        public vorname?: string,
        public nachname?: string
    ) { }
}
