import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, RouterLinkActive } from '@angular/router';
import { LoginViewComponent } from '../../components/login-view/login-view.component';
import { ChatViewComponent } from '../../components/chat-view/chat-view.component';
import { RegisterViewComponent } from '../../components/register-view/register-view.component';
import { PersonSearchViewComponent } from '../../components/person-search-view/person-search-view.component';
import { ContactListViewComponent } from '../../components/contact-list-view/contact-list-view.component';
import { ChatListViewComponent } from '../../components/chat-list-view/chat-list-view.component';
import { BlockedContactListViewComponent } from '../../components/blocked-contact-list-view/blocked-contact-list-view.component';

const routes: Routes = [

  {
    path: 'login',
    component: LoginViewComponent,
  },
  {
    path: '',
    component: LoginViewComponent,
  },
  {
    path: 'chat',
    component: ChatViewComponent,
  },
  {
    path: 'register',
    component: RegisterViewComponent,
  },
  {
    path: 'person-search',
    component: PersonSearchViewComponent,
  },
  {
    path: 'contact-list',
    component: ContactListViewComponent,
  },
  {
    path: 'chat-list',
    component: ChatListViewComponent,
  },
  {
    path: 'blocked-contacts',
    component: BlockedContactListViewComponent,
  }

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class RoutingModule { }
