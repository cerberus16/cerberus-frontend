import { Injectable } from '@angular/core';
import { Message } from '../models/Message';
import { Chat } from '../models/Chat';

@Injectable()
export class ChatListService {

  chatListCache: Chat[];
  globalChat: Message[];

  constructor() {

  }

  saveChatListCache(messages: Message[]) {
    let chats = new Array<Chat>();

    let names = messages.map(m => m.currentUser);
    messages.map(m => m.otherUser).forEach(n => names.push(n));
    let uniquenames = new Set<String>(names);

    uniquenames.forEach(n => {
      let chat = new Chat('Abdi', n, messages.filter(m => m.otherUser === n || m.currentUser === n));
      chats.push(chat);
      // messages.filter(m => m.otherUser === n || m.currentUser === n);
    });


    this.chatListCache = chats;

  }
}



