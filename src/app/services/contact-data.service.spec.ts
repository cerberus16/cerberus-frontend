import { TestBed, inject } from '@angular/core/testing';

import { ContactDataService } from './contact-data.service';
import { AppModule } from '../app.module';
import { RestApiServiceService } from './rest-api-service.service';
import { BlockedContactListViewComponent } from '../components/blocked-contact-list-view/blocked-contact-list-view.component';
import { ContactListViewComponent } from '../components/contact-list-view/contact-list-view.component';
import { FormsModule } from '@angular/forms';

describe('ContactDataService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [AppModule, FormsModule]
        });
    });

    it('should be created', inject([ContactDataService], (service: ContactDataService) => {
        expect(service).toBeTruthy();
    }));
});
