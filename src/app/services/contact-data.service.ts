import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Contact } from '../models/contact';
import { User } from '../models/user';
import { RestApiServiceService } from './rest-api-service.service';

@Injectable()
export class ContactDataService {

    public contacts: Contact[] = [];
    private exampleContacts: Contact[] = [];


    constructor() {
        //eingeloggter User
        this.exampleContacts.push(new Contact('Mohsii', false, 'eddie'));
        this.exampleContacts.push(new Contact('Lukas', false, 'eddie'));

        if (JSON.parse(localStorage.getItem('contacts')) === null) {
            localStorage.setItem('contacts', JSON.stringify(this.contacts));
        }
    }

    blocked(_contact: Contact) {
        _contact.blocked = true;
        this.updateLocalStorageArray();
    }

    updateLocalStorageArray() {
        localStorage.setItem('contacts', JSON.stringify(this.contacts));
        console.log('[DEBUG] Contacts in localStorage updated');
    }

    noContacts(): boolean {
        let noContacts;
        if (this.contacts.length < 1) {
            noContacts = true;
        } else {
            noContacts = false;
        }
        return noContacts;
    }
}