import { TestBed, inject } from '@angular/core/testing';

import { LoginserviceService } from './loginservice.service';
import { AppModule } from '../app.module';

describe('LoginserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppModule]
    });
  });

  it('should be created', inject([LoginserviceService], (service: LoginserviceService) => {
    expect(service).toBeTruthy();
  }));
});
