import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Router } from '@angular/router';

@Injectable()
export class LoginserviceService {

  user: User;

  constructor(private router: Router) {

  }

  login(user: User) {
    this.user = user;
    this.router.navigateByUrl('chat');
  }

  unlog() {
    this.user = new User();
    this.router.navigateByUrl('login');
  }

}
