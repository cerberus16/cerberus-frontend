import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { RestApiServiceService } from './rest-api-service.service';
import { AppModule } from '../app.module';
import { Contact } from '../models/contact';
import { User } from '../models/user';

describe('RestApiServiceService', () => {

  let service: RestApiServiceService;
  let http: HttpClient;
  let backend: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, HttpClientModule, HttpClientTestingModule]
    });

    service = TestBed.get(RestApiServiceService);
    http = TestBed.get(HttpClient);
    backend = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get a Chatlist', () => {
    let user = new User();
    user.username = "max";
    spyOn(service, 'transformJsonToChatArray');
    service.Chatlist(user).then(() => {
      expect(service.transformJsonToChatArray).toHaveBeenCalled();
    });

    backend.expectOne('api/chats/max').flush({}, { status: 200, statusText: "foo" });
  });


  // Chatlist(user: User): any[] {
  //   console.log("CHatlist m Service aufgerufen")
  //   this.http.get('api/chats/' + user.username).subscribe(data => {
  //     console.log(data);
  //     if (data) {
  //       //  this.chatCache = data;
  //       this.transformJsonToChatArray(data);

  //       return data;
  //     }
  //     return null;
  //   });
  //   return null;
  // }


  // let contact: Contact = new Contact("max", false, "moritz");
  // spyOn(service, 'refresh');
  // service.saveContact(contact);
  // done();
  // expect(service.refresh).toHaveBeenCalled();

  // backend.expectOne('api/users/max').flush({}, { status: 200, statusText: "foo" });
  // backend.expectOne('api/contacts').flush({}, { status: 200, statusText: "foo" });
});
