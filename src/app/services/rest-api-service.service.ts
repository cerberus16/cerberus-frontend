import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';
import { RouterLink } from '@angular/router/src/directives/router_link';
import { Contact } from '../models/contact';
import { Chat } from '../models/Chat';
import { Data } from '@angular/router';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';
import { Message } from '../models/Message';
import { ChatListService } from './chat-list.service';
import { LoginserviceService } from './loginservice.service';
declare function require(url: string);
@Injectable()
export class RestApiServiceService {

  chatCache: Chat[];

  private options = {
    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
  };

  public config = require('assets/config.json');
  private user: User;
  public tmpUser: User;
  public contactsCache: User[];

  public refresh() {
  }

  constructor(private http: HttpClient, public chatListService: ChatListService, private loginserviceService: LoginserviceService) { }

  saveUser(user: User) {
    const body = new URLSearchParams();
    body.set('username', user.username);
    body.set('password', user.password);
    body.set('gebDat', user.gebDat.toString());
    body.set('vorname', user.vorname);
    body.set('nachname', user.nachname);
    console.log('body: ' + body.toString());
    this.http.post(
      `api/users/`,
      body.toString(),
      this.options).subscribe(() => this.refresh(),
        (err) => {
        });
  }


  saveContact(contact: Contact) {
    this.searchIfContactExists(contact).then(vorhanden => {
      console.log(1);
      if (vorhanden) {
        this.storeContactWithApi(contact);
      }
    });
    console.log(2);
  }


  storeContactWithApi(contact: Contact) {
    const body = new URLSearchParams();
    body.set('username', contact.username);
    body.set('usernameOwner', this.loginserviceService.user.username);
    body.set('blocked', contact.blocked.toString());

    return this.http.post(this.config.BackendURL + `contacts/`, body.toString(), this.options).toPromise()
      .then(() => {
        this.refresh()
      })
      .catch(e => {
        window.alert("User nicht gefunden. " + e.status);
      });
  }

  searchIfContactExists(contact: Contact) {
    return this.http.get(this.config.BackendURL + `users/` + contact.username, this.options).toPromise()
      .then((data) => {
        return true;
      })
      .catch(e => {
        return false;
      });
  }

  Chatlist(user: User) {
    console.log("Chatlist m Service aufgerufen")
    return this.http.get(this.config.BackendURL + 'chats/' + user.username).toPromise()
      .then(data => {
        console.log(data);
        if (data) {
          //  this.chatCache = data;
          this.transformJsonToChatArray(data);
        }
      })
      .catch(e => {
      });
  }

  getGlobal() {
    console.log("Chatlist m Service aufgerufen")
    return this.http.get(this.config.BackendURL + 'chat/').toPromise()
      .then(data => {
        console.log(data);
        if (data) {
          //  this.chatCache = data;
          this.transformJsonToGlobalChatArray(data);
        }
      })
      .catch(e => {
      });
  }

  transformJsonToGlobalChatArray(data: any) {
    let messages = new Array<Message>();

    data.forEach(row => {
      let message = new Message(row["User1"], row["User2"], row["Chat"], row["time"]);
      //console.log(row["Chat"]);
      messages.push(message);
    });

    console.log(messages);

    this.chatListService.globalChat = messages;
  }

  transformJsonToChatArray(data: any) {
    let messages = new Array<Message>();

    data.forEach(row => {
      let message = new Message(row["User1"], row["User2"], row["Chat"], row["time"]);
      //console.log(JSON.stringify(row));
      messages.push(message);
    });

    //console.log(JSON.stringify(data));
    //console.log(JSON.stringify(messages));
    this.chatListService.saveChatListCache(messages);
  }

  login(user: User, result?: Object) {
    try {
      this.http.get(this.config.BackendURL + 'users/' + user.username + '/' + user.password).subscribe(data => {
        if (data) {
          console.log('Login erfolgreich');
          result = data;
          this.loginserviceService.login(user);
        } if (!data) {
          console.log('Login fehlgeschlagen!');
        }
      });
    } catch (e) {
      console.log('Login fehlgeschlagen!');
    }
  }

  getUser(username: string): any {
    this.http.get(this.config.BackendURL + 'contacts/' + username).subscribe(data => {
      console.log(data);
      if (data) {
        console.log('Kontakt gefunden');
        data = this.user;
        return data;
      }
      return false;
    });
  }

  sendGlobalChat(msg: Message) {

    const body = new URLSearchParams();
    body.set('message', msg.text);
    body.set('User1', this.loginserviceService.user.username);
    body.set('User2', '*GLOBAL*');
    body.set('timestamp', Date.now() + '');

    return this.http.post(this.config.BackendURL + `chat/`, body.toString(), this.options).toPromise()
      .then(() => {
        this.refresh();
      })
      .catch(e => {
        //window.alert("Kontakt gefunden, aber konnte nicht gespeichert werden. " + e.status);
      });
  }
  showContact() {
    this.http.get(this.config.BackendURL + 'contacts/' + this.loginserviceService.user.username).subscribe(data => {
      console.log(JSON.stringify(data));
      if (data) {
        console.log('Kontakte gefunden');
        //data = this.user;
        this.contactsCache = <User[]>data;
      }
    });
  }
  delContact(user: String, result?: Object) {
    // tslint:disable-next-line:max-line-length
    this.http.post(this.config.BackendURL + 'contacts/' + user + '/' + this.loginserviceService.user.username, this.options).subscribe(data => {
      console.log(JSON.stringify(data));
      if (data) {
      }
    });
  }
}
