import { TestBed, inject } from '@angular/core/testing';

import { SocketCommunicationService } from './socket-communication.service';

describe('SocketCommunicationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SocketCommunicationService]
    });
  });

  it('should be created', inject([SocketCommunicationService], (service: SocketCommunicationService) => {
    expect(service).toBeTruthy();
  }));
});
