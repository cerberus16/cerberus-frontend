import { Injectable } from '@angular/core';
import * as io from "socket.io-client";
import { Message } from "../models/Message";
import { LoginserviceService } from "./loginservice.service";


declare function require(url: string);

@Injectable()
export class SocketCommunicationService {

  globalChatListCache: Message[];


  public config = require('assets/config.json');
  socket = io(this.config.BackendURL);
  constructor(private loginserviceService: LoginserviceService) {
    this.socket.on('new-message', function (data) {
      console.log("new Message: " + JSON.stringify(data));
      this.globalChatListCache = data;
    }.bind(this));

  }

  public postMessage(message: Message) {
    message.currentUser = this.loginserviceService.user.username;
    message.otherUser = '*GLOBAL*';
    message.time = new Date();
    this.socket.emit('save-message', { message: message });
  }

  public getChatList(message: Message) {
  }
}
